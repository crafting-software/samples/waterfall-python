[![pipeline status](https://gitlab.com/crafting-software/samples/waterfall-python/badges/main/pipeline.svg)](https://gitlab.com/crafting-software/samples/waterfall-python/-/commits/main)  [![coverage report](https://gitlab.com/crafting-software/samples/waterfall-python/badges/main/coverage.svg)](https://gitlab.com/crafting-software/samples/waterfall-python/-/commits/main)


# Waterfall

The aim of this project is to test a cascade of value-added processes.
