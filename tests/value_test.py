# -*- coding: utf-8 -*-
import pytest
from _pytest.python_api import ApproxBase  # no better way ti import ApproxBase
from hypothesis import assume, given
from hypothesis.strategies import integers

from waterfall.model import Value

numbers = integers(-10_000_000_000, 10_000_000_000)


def approx(value: Value) -> ApproxBase:
    return pytest.approx(value, 0.001)


@given(numbers, numbers)
def test_should_have_addition_operation(left: int, right: int) -> None:
    assert Value(left) + Value(right) == approx(Value(left + right))


@given(numbers, numbers)
def test_should_have_substraction_operation(left: int, right: int) -> None:
    assert Value(left) - Value(right) == approx(Value(left - right))


@given(numbers, numbers)
def test_should_have_multiplication_operation_with_number(
    left: int, right: int
) -> None:
    assert Value(left) * right == approx(Value(left * right))


@given(numbers, numbers)
def test_should_have_division_operation_with_number(left: int, right: int) -> None:
    assume(right != 0)
    assert Value(left) / right == approx(Value(float(left) / right))


@given(numbers)
def test_should_have_equal_operation(value: int) -> None:
    assert Value(value) == Value(value)


@given(numbers, numbers)
def test_should_have_not_equal_operation(left: int, right: int) -> None:
    assume(left != right)
    assert Value(left) != Value(right)


@given(numbers, numbers)
def test_should_have_grether_than_operation(left: int, right: int) -> None:
    assume(left > right)
    assert Value(left) > Value(right)


@given(numbers, numbers)
def test_should_have_grether_or_equal_operation(left: int, right: int) -> None:
    assume(left >= right)
    assert Value(left) >= Value(right)


@given(numbers, numbers)
def test_should_have_less_than_operation(left: int, right: int) -> None:
    assume(left < right)
    assert Value(left) < Value(right)


@given(numbers, numbers)
def test_should_have_less_or_equal_operation(left: int, right: int) -> None:
    assume(left <= right)
    assert Value(left) <= Value(right)
