SOURCES_FILES = $(shell find waterfall -type f -name '*.py')
TEST_FILES = $(shell find tests -type f -name '*.py')
SCRIPTS = demo
PYTHON_FILES = $(SOURCES_FILES) $(TEST_FILES) $(SCRIPTS)

init:
	pip install -r requirements.txt

format:
	@black --check $(PYTHON_FILES)
	@isort --check --diff $(PYTHON_FILES)

typecheck:
	@mypy \
		--install-types \
		--non-interactive \
		--show-error-codes \
		--linecount-report .report \
		$(PYTHON_FILES)

lint:
	@pylint -j 4 $(PYTHON_FILES)

check: format typecheck lint

test:
	@pytest --hypothesis-profile=dev -m "not slow"

all-test:
	@pytest --hypothesis-profile=dev

debug-test:
	@pytest -vv --hypothesis-profile=dev

coverage:
	@pytest --hypothesis-profile=dev --cov=waterfall --cov-report=html:.htmlcov

reformat:
	@black $(PYTHON_FILES)
	@isort $(PYTHON_FILES)
