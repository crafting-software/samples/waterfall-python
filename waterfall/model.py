# -*- coding: utf-8 -*-
from __future__ import annotations

from abc import ABC, abstractmethod
from collections.abc import Iterable
from dataclasses import dataclass
from itertools import pairwise
from math import ceil

from prettytable import PrettyTable


@dataclass(frozen=True)
class Value:
    value: float

    def __add__(self, right: Value) -> Value:
        return Value(self.value + right.value)

    def __sub__(self, right: Value) -> Value:
        return Value(self.value - right.value)

    def __mul__(self, right: int | float) -> Value:
        return Value(ceil(self.value * right))

    def __truediv__(self, right: int | float) -> Value:
        return Value(self.value / right)

    def __floordiv__(self, right: int | float) -> Value:
        return Value(ceil(self.value // right))

    def __eq__(self, right: object) -> bool:
        if isinstance(right, Value):
            return self.value == right.value
        return NotImplemented

    def __ne__(self, right: object) -> bool:
        if isinstance(right, Value):
            return self.value != right.value
        return NotImplemented

    def __lt__(self, right: Value) -> bool:
        return self.value < right.value

    def __le__(self, right: Value) -> bool:
        return self.value <= right.value

    def __gt__(self, right: Value) -> bool:
        return self.value > right.value

    def __ge__(self, right: Value) -> bool:
        return self.value >= right.value

    def __repr__(self) -> str:
        return f"EUR {self.value:_.2f}"


class WaterfallItem(ABC):

    @abstractmethod
    def identifier(self) -> str:
        pass

    @abstractmethod
    def go_down(self, value: Value) -> Value:
        pass

    @abstractmethod
    def go_up(self, value: Value) -> Value:
        pass

    @abstractmethod
    def describe(self) -> str:
        pass


class ConstItem(WaterfallItem):

    def __init__(self, identifier: str):
        self.__identifier = identifier

    def identifier(self) -> str:
        return self.__identifier

    def go_down(self, value: Value) -> Value:
        return value

    def go_up(self, value: Value) -> Value:
        return value

    def describe(self) -> str:
        return "-"

    def __repr__(self) -> str:
        return f"{self.__identifier}"


class PercentDiscountItem(WaterfallItem):

    def __init__(self, identifier: str, percent: float):
        self.__identifier = identifier
        self.__percent = percent

    def identifier(self) -> str:
        return self.__identifier

    def go_down(self, value: Value) -> Value:
        return value * (1 - self.__percent)

    def go_up(self, value: Value) -> Value:
        return value / (1 - self.__percent)

    def describe(self) -> str:
        return f"discount of {self.__percent*100}%"

    def __repr__(self) -> str:
        return f"{self.__identifier} - discount of {self.__percent*100}%"


class FixedDiscountItem(WaterfallItem):

    def __init__(self, identifier: str, amount: Value):
        self.__identifier = identifier
        self.__amount = amount

    def identifier(self) -> str:
        return self.__identifier

    def go_down(self, value: Value) -> Value:
        return value - self.__amount

    def go_up(self, value: Value) -> Value:
        return value + self.__amount

    def describe(self) -> str:
        return f"discount of {self.__amount}"

    def __repr__(self) -> str:
        return f"{self.__identifier} - discount of {self.__amount}"


@dataclass(frozen=True)
class Solution:
    items: tuple[WaterfallItem, ...]
    values: dict[str, Value]

    def render(self) -> str:
        table = PrettyTable()
        table.align = "l"
        table.field_names = ["Label", "Rule", "Amount"]
        for item in self.items:
            table.add_row(
                [
                    item.identifier(),
                    item.describe(),
                    repr(self.values[item.identifier()]),
                ]
            )
        return str(table)


class Waterfall:
    def __init__(self, *items: WaterfallItem):
        self.__items = items

    def solve(self, data: dict[str, Value]) -> Solution:
        return Solver(self.__items, data).propagate().solution()

    def describe(self) -> str:
        table = PrettyTable()
        table.align = "l"
        table.field_names = ["Label", "Rule"]
        for item in self.__items:
            table.add_row([item.identifier(), item.describe()])
        return str(table)


class Solver:
    def __init__(self, items: tuple[WaterfallItem, ...], data: dict[str, Value]):
        self.__items = items
        self.__data = data
        self.__by_identifier = {}
        self.__previous = {}
        self.__next = {}
        for first, second in pairwise(items):
            self.__next[first.identifier()] = second
            self.__previous[second.identifier()] = first
        for item in items:
            self.__by_identifier[item.identifier()] = item

    def propagate(self) -> Solver:
        while not self.is_solved():
            for item in self.valuated_items():
                previous = self.previous_item(item)
                if previous is not None and not self.has_value(previous):
                    self.value_for(previous, item.go_up(self.value_of(item)))
                    continue
                next_ = self.next_item(item)
                if next_ is not None and not self.has_value(next_):
                    self.value_for(next_, next_.go_down(self.value_of(item)))
                    continue
        return self

    def solution(self) -> Solution:
        return Solution(self.__items, dict(self.__data))

    def is_solved(self) -> bool:
        for item in self.__items:
            if not self.has_value(item):
                return False
        return True

    def value_for(self, item: WaterfallItem, value: Value) -> None:
        self.__data[item.identifier()] = value

    def value_of(self, item: WaterfallItem) -> Value:
        return self.__data[item.identifier()]

    def has_value(self, item: WaterfallItem) -> bool:
        return item.identifier() in self.__data

    def previous_item(self, item: WaterfallItem) -> WaterfallItem | None:
        return self.__previous.get(item.identifier(), None)

    def next_item(self, item: WaterfallItem) -> WaterfallItem | None:
        return self.__next.get(item.identifier(), None)

    def valuated_items(self) -> Iterable[WaterfallItem]:
        return [self.__by_identifier[i] for i in self.__data]
